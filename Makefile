.DEFAULT_GOAL := deploy

build1:
	docker build  . -t reto_devops:latest

build2:
	cd proxy && docker build  . -t reto_devops_proxy:latest

deploy:
	cd kubernetes && kubectl apply -f kubernetes